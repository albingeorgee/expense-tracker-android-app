package com.example.expensetracker;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

/**
 * Created by striker on 9/7/13.
 */
public class EditExpenseActivity extends Activity {
    ExpensesDataSources dataSources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_expense);

        String expenseFor = getIntent().getStringExtra("expenseFor");
        String desc = getIntent().getStringExtra("desc");
        String date = getIntent().getStringExtra("date");
        int amount = getIntent().getIntExtra("amount", 0);

        EditText et = (EditText)findViewById(R.id.expenseFor);
        et.setText(expenseFor);
        et = (EditText)findViewById(R.id.desc);
        et.setText(desc);
        et = (EditText)findViewById(R.id.date);
        et.setText(date);
        et = (EditText)findViewById(R.id.amount);
        et.setText(Integer.toString(amount));
    }
}
