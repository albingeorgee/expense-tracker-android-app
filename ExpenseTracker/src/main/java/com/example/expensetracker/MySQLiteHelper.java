package com.example.expensetracker;

import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by striker on 2/7/13.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {


    public static final String TABLE_EXPENSES = "expenses";
    public static final String EXPENSES_ID = "_id";
    public static final String EXPENSES_EXPENSE_FOR = "expensefor";
    public static final String EXPENSES_AMOUNT = "amount";
    public static final String EXPENSES_DESCRIPTION = "desc";
    public static final String EXPENSES_DATE = "date";

    private static final String DATABASE_NAME = "expense.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_EXPENSES + "(" + EXPENSES_ID
            + " integer primary key autoincrement, " + EXPENSES_EXPENSE_FOR
            + " text not null, " + EXPENSES_AMOUNT
            + " number(2), " + EXPENSES_DESCRIPTION
            + " text, " + EXPENSES_DATE
            + " datetime default CURRENT_TIMESTAMP);";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSES);
        onCreate(db);
    }
}
