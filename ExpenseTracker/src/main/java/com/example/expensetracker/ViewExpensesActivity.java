package com.example.expensetracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

/**
 * Created by striker on 4/7/13.
 */
public class ViewExpensesActivity extends Activity {

    ExpensesDataSources dataSources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_expenses);

        dataSources = new ExpensesDataSources(this);
        dataSources.open();

        TableLayout tl = (TableLayout)findViewById(R.id.tableView);

        dataSources.open();

        List<Expenses> expenses = dataSources.getAllExpenses();

        for(Expenses expense : expenses) {
            final String expenseFor = expense.getExpenseFor();
            final int amount = expense.getAmount();
            final String desc = expense.getDesc();
            final Date date = expense.getDate();

            TableRow row = new TableRow(this);
            TableLayout.LayoutParams lp = new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.FILL_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT );
            row.setLayoutParams(lp);
            row.setPadding(15, 3, 15, 3);

            TextView text = new TextView(this);
            text.append("Expense For: " + expenseFor + "\n");
            text.append("Amount: " + amount + "\n");
            text.append("Description: " + desc + "\n");
            text.append("Date: " + date.toString() + "\n");
            text.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent myIntent = new Intent(getBaseContext(), EditExpenseActivity.class);
                    myIntent.putExtra("expenseFor", expenseFor);
                    myIntent.putExtra("desc", desc);
                    myIntent.putExtra("date", date.toString());
                    myIntent.putExtra("amount", amount);
                    ViewExpensesActivity.this.startActivity(myIntent);
//                    startActivity(new Intent(getBaseContext(), EditExpenseActivity.class));

                }
            });

            tl.addView(text);
        }
    }

    @Override
    protected void onResume() {
        dataSources.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        dataSources.close();
        super.onPause();
    }


}
