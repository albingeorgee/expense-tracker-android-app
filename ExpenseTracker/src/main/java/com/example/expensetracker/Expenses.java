package com.example.expensetracker;

import java.util.Date;

/**
 * Created by striker on 2/7/13.
 */
public class Expenses {

    public long id;
    public String expenseFor;
    public int amount;
    public String desc;
    public Date date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getExpenseFor() {
        return expenseFor;
    }

    public void setExpenseFor(String expenseFor) {
        this.expenseFor = expenseFor;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
