package com.example.expensetracker;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by striker on 1/7/13.
 */
public class AddExpenseActivity extends Activity {

    ExpensesDataSources dataSources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_expense);
        RelativeLayout layout = (RelativeLayout)findViewById(R.id.base);
        dataSources = new ExpensesDataSources(this);
        dataSources.open();
    }

    @Override
    protected void onResume() {
        dataSources.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        dataSources.close();
        super.onPause();
    }


    public void addExpense(View view){

        EditText editTextField = (EditText)findViewById(R.id.expenseFor);
        String expenseFor = editTextField.getText().toString();
        TextView editText = (TextView) findViewById(R.id.amount);
        int amount = Integer.parseInt(editText.getText().toString());
        editTextField = (EditText)findViewById(R.id.description);
        String desc = editTextField.getText().toString();
        Expenses expense = dataSources.createExpense(expenseFor, amount, desc);


        Context context = getApplicationContext();
        CharSequence text = "Values Added!!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        Intent myIntent = new Intent(AddExpenseActivity.this, MainActivity.class);
        AddExpenseActivity.this.startActivity(myIntent);
    }

}
