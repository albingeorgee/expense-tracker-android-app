package com.example.expensetracker;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
/**
 * Created by striker on 2/7/13.
 */
public class ExpensesDataSources {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;

    private String[] allColumns = { MySQLiteHelper.EXPENSES_ID,
            MySQLiteHelper.EXPENSES_EXPENSE_FOR,
            MySQLiteHelper.EXPENSES_AMOUNT,
            MySQLiteHelper.EXPENSES_DESCRIPTION,
            MySQLiteHelper.EXPENSES_DATE };

    private Expenses cursorToExpense(Cursor cursor) {
        Expenses expense = new Expenses();
        expense.setId(cursor.getLong(0));
        expense.setExpenseFor(cursor.getString(1));
        expense.setAmount(cursor.getInt(2));
        expense.setDesc(cursor.getString(3));
        try{
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(cursor.getString(4));
            expense.setDate(date);
        }catch(Exception e){
            System.out.println("Date not fetched !!!");
        }

        return expense;
    }

    public ExpensesDataSources(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Expenses createExpense(String expenseFor, int amount, String desc) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.EXPENSES_EXPENSE_FOR, expenseFor);
        values.put(MySQLiteHelper.EXPENSES_AMOUNT, amount);
        values.put(MySQLiteHelper.EXPENSES_DESCRIPTION, desc);
        long insertId = database.insert(MySQLiteHelper.TABLE_EXPENSES, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_EXPENSES,
                allColumns, MySQLiteHelper.EXPENSES_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Expenses newExpense = cursorToExpense(cursor);
        cursor.close();
        return newExpense;
    }


    public List<Expenses> getAllExpenses() {
        List<Expenses> expenses = new ArrayList<Expenses>();

        Cursor cursor = database.query(MySQLiteHelper.TABLE_EXPENSES,
                allColumns, null, null, null, null, null);


        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Expenses expense = cursorToExpense(cursor);
            expenses.add(expense);
            cursor.moveToNext();
        }
        cursor.close();

        return expenses;
    }

    public void deleteComment(Expenses expense) {
        long id = expense.getId();
        System.out.println("Expense deleted with id: " + id);
        database.delete(MySQLiteHelper.TABLE_EXPENSES, MySQLiteHelper.EXPENSES_ID
                + " = " + id, null);
    }
}

